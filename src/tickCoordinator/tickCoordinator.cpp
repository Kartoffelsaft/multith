#include <assert.h>

#include "./tickCoordinator.hpp"

void TickCoordinator::giveOutboundActors(
    std::weak_ptr<Actor<WindowHandler>> nprinter,
    std::weak_ptr<Actor<StateHandler>> nstate
){
    outboundActors = OutboundActors{
        nprinter,
        nstate,
    };
}

TickCoordinator::~TickCoordinator()
{}

void TickCoordinator::tickLoop()
{
    assert(outboundActors.has_value());
    lastTick = std::chrono::steady_clock::now();
    ActorReturn<bool> running;
    do
    {
        outboundActors->sendTicks();
        running = outboundActors->state.lock()->call(&StateHandler::isRunning);

        auto now = std::chrono::steady_clock::now();

        while(now < lastTick + TICK_TIME)
        {
            now = std::chrono::steady_clock::now();
            std::this_thread::sleep_for(lastTick + TICK_TIME - now);
        }

        lastTick += TICK_TIME;
    }
    while(running.get());
}

void TickCoordinator::OutboundActors::sendTicks()
{
    printer.lock()->call(&WindowHandler::onTick);
}
